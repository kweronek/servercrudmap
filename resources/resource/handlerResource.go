package resource

import (
	"api-1/resources/resource/verbs"
	"api-1/serviceGlobals/funcs"

	"api-1/serviceGlobals"
	_ "api-1/serviceGlobals"
	//	"api-1/resources/resource/funcs"
	_ "api-1/serviceGlobals/funcs"
	"encoding/json"
	"fmt"
	"helper"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

// ********************************************************************************************************************
// handler for resources without ID
// ********************************************************************************************************************
func ListHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])

	switch r.Method {

	case "GET":
		result, httpStatus := verbs.GetAllResources()
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprintf(w, result)
		}
	case "POST":
		requestBody, httpStatus := serviceGlobals.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			result, httpStatus := verbs.PostResource(requestBody)
			w.WriteHeader(httpStatus)
			fmt.Fprint(w, result)
		}

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Print("** method ", r.Method, " not supported without ID")
	}
}

// ********************************************************************************************************************
// Handler for resources with ID specified
// ********************************************************************************************************************
func HandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
	var pp = utils.ParsePath(r.URL.Path)

	ID, err := strconv.Atoi(pp[1])
	if err != nil {
		if pp[1] == "" {
			log.Print(pp[1] + "** no resources specified, redirected to -->")
			HandleFunc(w, r)
		} else {
			log.Print("** \"" + pp[1] + "\" is not a number")
			w.WriteHeader(http.StatusBadRequest)
		}
		return
	}

	switch r.Method {
	case "GET": // read a single resources
		resourceBuf, httpStatus := verbs.GetResource(ID)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusNotFound)
		} else {

			switch r.Header.Get("Accept") {

			case "": // this order leads to better performance
				fallthrough
			case "application/json":
				fallthrough
			default:
				w.Header().Set("Content-Type", "application/json")
				jsonData, err := json.Marshal(resourceBuf)
				if err != nil {
					log.Print(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				fmt.Fprint(w, string(jsonData))

			case "text/html":
				w.Header().Set("Content-Type", "text/html; charset=utf-8")
				t, err := template.ParseFiles("views/resources.html")
				if err != nil {
					log.Print(w, "Unable to load template")
				} else {
					err = t.Execute(w, resourceBuf)
					if err != nil {
						log.Print(err)
					}
				}
			case "text/plain":
				w.Header().Set("Content-Type", "text/plain")
				fmt.Fprint(w, resourceBuf)
			}
		}

	case "POST": // create a new resource
		var pp = utils.ParsePath(r.URL.Path)
		if pp[1] != "" {
			w.WriteHeader(http.StatusBadRequest)
			log.Print("** bad request")
			return
		}

		var requestBody, httpStatus = serviceGlobals.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print("** httpStatus: ", httpStatus)
		} else {
			switch r.Header.Get("Content-Type") {
			case "":
				fallthrough
			case "application/json":
				newID, httpStatus := verbs.PostResource(requestBody)
				w.WriteHeader(httpStatus)
				fmt.Println(newID)
				fmt.Fprint(w, "{\nnew ID:\t", newID, "\n}")
			default:
				w.WriteHeader(http.StatusUnsupportedMediaType)
				log.Print("** unsupported media type")
			}
		}

	case "PUT": // replace a resource completely
		var pp = utils.ParsePath(r.URL.Path)
		pID, err := strconv.Atoi(pp[1])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		switch r.Header.Get("Content-Type") {
		case "", "application/json":
			requestBody, httpStatus := serviceGlobals.ReqBody(r)
			if httpStatus != 200 {
				w.WriteHeader(httpStatus)
				return
			} else {
				httpStatus = verbs.PutResource(pID, requestBody)
				w.WriteHeader(httpStatus)
			}
		default:
			w.WriteHeader(http.StatusUnsupportedMediaType)
			log.Print("** unsupported media type")
		}

	case "PATCH": // change parts of a resource
		var pp = utils.ParsePath(r.URL.Path)
		pID, err := strconv.Atoi(pp[1])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var requestBody, httpStatus = serviceGlobals.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(httpStatus)
			return
		} else {
			switch r.Header.Get("Content-Type") {
			case "", "application/json":
				httpStatus := verbs.PatchResource(pID, requestBody)
				w.WriteHeader(httpStatus)
			default:
				w.WriteHeader(http.StatusUnsupportedMediaType)
				log.Print("** unsupported media type")
			}
		}

	case "DELETE": // delete a resources
		if funcs.CheckResource(ID) {
			verbs.DeleteResource(ID)
		} else {
			log.Println("** resources ", ID, "does not exist!")
		}

	default: // all unsupported methods
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Println("** method: " + r.Method + "not supported for this URI!\n")
	}
}
