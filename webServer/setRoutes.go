package webServer

import (
	"api-1/resources/about"
	"api-1/resources/home"
	"api-1/resources/resource"
	thing "api-1/resources/things"
	"net/http"
)

func SetRoutesResource() {
	//	s.router.HandleFunc("/api/", s.handleAPI())
	//	s.router.HandleFunc("/about", s.handleAbout())
	//	s.router.HandleFunc("/", s.handleIndex())

	http.HandleFunc("/resources", resource.ListHandleFunc)
	http.HandleFunc("/resources/", resource.HandleFunc)
	http.HandleFunc("/things", thing.ListHandleFunc)
	http.HandleFunc("/things/", thing.HandleFunc)
	http.HandleFunc("/", home.Home)
	http.HandleFunc("/about", about.About)
	http.HandleFunc("/About", about.About)
}
