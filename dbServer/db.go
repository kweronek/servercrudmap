package dbServer

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB
var err error

func ConnectDatabase() {
	db, err = sql.Open("mysql", "root:root@/test")
	if err == nil {
		fmt.Println("Database connected!\n")
	} else {
		fmt.Println("Database not connected!\n")
	}
}
